Module                  Size  Used by
cpuid                  12288  0
xt_conntrack           12288  1
nft_chain_nat          12288  3
xt_MASQUERADE          16384  1
nf_nat                 61440  2 nft_chain_nat,xt_MASQUERADE
nf_conntrack_netlink    57344  0
nf_conntrack          200704  4 xt_conntrack,nf_nat,nf_conntrack_netlink,xt_MASQUERADE
nf_defrag_ipv6         24576  1 nf_conntrack
nf_defrag_ipv4         12288  1 nf_conntrack
xfrm_user              57344  1
xfrm_algo              20480  1 xfrm_user
xt_addrtype            12288  2
nft_compat             20480  4
nf_tables             368640  57 nft_compat,nft_chain_nat
nfnetlink              20480  4 nft_compat,nf_conntrack_netlink,nf_tables
br_netfilter           32768  0
bridge                405504  1 br_netfilter
stp                    12288  1 bridge
llc                    16384  2 bridge,stp
overlay               208896  0
sunrpc                802816  1
binfmt_misc            24576  1
nls_iso8859_1          12288  2
intel_rapl_msr         20480  0
intel_rapl_common      36864  1 intel_rapl_msr
intel_uncore_frequency    16384  0
intel_uncore_frequency_common    16384  1 intel_uncore_frequency
i10nm_edac             32768  0
nfit                   81920  1 i10nm_edac
x86_pkg_temp_thermal    16384  0
intel_powerclamp       24576  0
coretemp               24576  0
kvm_intel             471040  0
ipmi_ssif              45056  0
kvm                  1396736  1 kvm_intel
irqbypass              12288  1 kvm
rapl                   20480  0
intel_cstate           24576  0
cmdlinepart            12288  0
spi_nor               159744  0
mei_me                 53248  0
isst_if_mmio           12288  0
input_leds             12288  0
joydev                 32768  0
isst_if_mbox_pci       12288  0
mtd                   102400  3 spi_nor,cmdlinepart
isst_if_common         24576  2 isst_if_mmio,isst_if_mbox_pci
mei                   172032  1 mei_me
intel_vsec             20480  0
intel_pch_thermal      20480  0
ioatdma                81920  0
acpi_ipmi              20480  0
ipmi_si                86016  1
ipmi_devintf           16384  0
ipmi_msghandler        86016  4 ipmi_devintf,ipmi_si,acpi_ipmi,ipmi_ssif
acpi_power_meter       20480  0
acpi_pad              184320  0
mac_hid                12288  0
sch_fq_codel           24576  256
dm_multipath           45056  0
scsi_dh_rdac           16384  0
scsi_dh_emc            12288  0
scsi_dh_alua           20480  0
msr                    12288  0
parport_pc             53248  0
ppdev                  24576  0
lp                     28672  0
parport                77824  3 parport_pc,lp,ppdev
efi_pstore             12288  0
ip_tables              32768  0
x_tables               65536  5 xt_conntrack,nft_compat,xt_addrtype,ip_tables,xt_MASQUERADE
autofs4                57344  2
btrfs                1970176  0
blake2b_generic        24576  0
raid10                 73728  0
raid456               188416  0
async_raid6_recov      20480  1 raid456
async_memcpy           16384  2 raid456,async_raid6_recov
async_pq               20480  2 raid456,async_raid6_recov
async_xor              16384  3 async_pq,raid456,async_raid6_recov
async_tx               16384  5 async_pq,async_memcpy,async_xor,raid456,async_raid6_recov
xor                    20480  2 async_xor,btrfs
raid6_pq              126976  4 async_pq,btrfs,raid456,async_raid6_recov
libcrc32c              12288  5 nf_conntrack,nf_nat,btrfs,nf_tables,raid456
raid1                  61440  0
raid0                  24576  0
nvme                   57344  0
nvme_core             188416  1 nvme
ses                    20480  0
enclosure              24576  1 ses
rndis_host             20480  0
cdc_ether              24576  1 rndis_host
usbnet                 61440  2 rndis_host,cdc_ether
mii                    20480  1 usbnet
hid_generic            12288  0
usbhid                 77824  0
hid                   176128  2 usbhid,hid_generic
ast                   106496  0
drm_shmem_helper       24576  1 ast
drm_kms_helper        270336  2 ast
dax_hmem               16384  0
crct10dif_pclmul       12288  1
crc32_pclmul           12288  0
polyval_clmulni        12288  0
polyval_generic        12288  1 polyval_clmulni
ghash_clmulni_intel    16384  0
sha512_ssse3           49152  0
sha256_ssse3           32768  0
cxl_acpi               24576  0
sha1_ssse3             32768  0
cxl_core              286720  1 cxl_acpi
igb                   311296  0
mpt3sas               372736  0
ahci                   49152  5
raid_class             12288  1 mpt3sas
i2c_i801               36864  0
spi_intel_pci          12288  0
xhci_pci               24576  0
dca                    16384  2 igb,ioatdma
drm                   757760  4 drm_kms_helper,ast,drm_shmem_helper
i40e                  622592  0
scsi_transport_sas     57344  2 ses,mpt3sas
spi_intel              32768  1 spi_intel_pci
i2c_smbus              16384  1 i2c_i801
libahci                53248  1 ahci
i2c_algo_bit           16384  2 igb,ast
xhci_pci_renesas       16384  1 xhci_pci
aesni_intel           356352  0
crypto_simd            16384  1 aesni_intel
cryptd                 24576  2 crypto_simd,ghash_clmulni_intel
