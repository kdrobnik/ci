Module                  Size  Used by
xe                   2158592  0
drm_gpuvm              24576  1 xe
drm_exec               16384  1 xe
drm_buddy              20480  1 xe
gpu_sched              61440  1 xe
drm_suballoc_helper    12288  1 xe
drm_ttm_helper         12288  1 xe
ttm                    94208  2 drm_ttm_helper,xe
drm_display_helper    208896  1 xe
cec                    61440  2 drm_display_helper,xe
drm_kms_helper        241664  2 drm_display_helper,xe
drm                   696320  10 gpu_sched,drm_kms_helper,drm_exec,drm_suballoc_helper,drm_display_helper,drm_buddy,drm_ttm_helper,xe,ttm
video                  73728  1 xe
xt_conntrack           12288  1
xt_MASQUERADE          16384  1
nf_conntrack_netlink    53248  0
nfnetlink              20480  2 nf_conntrack_netlink
xfrm_user              53248  1
xfrm_algo              20480  1 xfrm_user
iptable_nat            12288  1
nf_nat                 57344  2 iptable_nat,xt_MASQUERADE
nf_conntrack          184320  4 xt_conntrack,nf_nat,nf_conntrack_netlink,xt_MASQUERADE
nf_defrag_ipv6         24576  1 nf_conntrack
nf_defrag_ipv4         12288  1 nf_conntrack
xt_addrtype            12288  2
iptable_filter         12288  1
bpfilter               12288  0
br_netfilter           32768  0
bridge                348160  1 br_netfilter
stp                    12288  1 bridge
llc                    16384  2 bridge,stp
overlay               176128  0
nls_iso8859_1          12288  2
dm_multipath           40960  0
scsi_dh_rdac           16384  0
scsi_dh_emc            12288  0
scsi_dh_alua           20480  0
x86_pkg_temp_thermal    16384  0
intel_powerclamp       16384  0
coretemp               16384  0
kvm_intel             376832  0
binfmt_misc            24576  1
kvm                  1216512  1 kvm_intel
snd_sof_pci_intel_lnl    12288  0
snd_sof_intel_hda_common   196608  1 snd_sof_pci_intel_lnl
snd_soc_hdac_hda       24576  1 snd_sof_intel_hda_common
soundwire_intel        65536  1 snd_sof_intel_hda_common
soundwire_generic_allocation    12288  1 soundwire_intel
snd_sof_intel_hda_mlink    40960  2 soundwire_intel,snd_sof_intel_hda_common
soundwire_cadence      40960  1 soundwire_intel
soundwire_bus         102400  3 soundwire_intel,soundwire_generic_allocation,soundwire_cadence
snd_sof_intel_hda      24576  1 snd_sof_intel_hda_common
snd_sof_pci            24576  2 snd_sof_pci_intel_lnl,snd_sof_intel_hda_common
snd_sof_xtensa_dsp     12288  1 snd_sof_intel_hda_common
snd_sof               319488  3 snd_sof_pci,snd_sof_intel_hda_common,snd_sof_intel_hda
snd_sof_utils          16384  1 snd_sof
snd_hda_ext_core       36864  4 snd_sof_intel_hda_common,snd_soc_hdac_hda,snd_sof_intel_hda_mlink,snd_sof_intel_hda
snd_soc_acpi_intel_match    90112  2 snd_sof_pci_intel_lnl,snd_sof_intel_hda_common
snd_soc_acpi           16384  2 snd_soc_acpi_intel_match,snd_sof_intel_hda_common
snd_intel_dspcfg       36864  2 snd_sof,snd_sof_intel_hda_common
snd_intel_sdw_acpi     16384  2 snd_sof_intel_hda_common,snd_intel_dspcfg
snd_hda_codec         196608  2 snd_soc_hdac_hda,snd_sof_intel_hda
snd_hda_core          135168  5 snd_hda_ext_core,snd_hda_codec,snd_sof_intel_hda_common,snd_soc_hdac_hda,snd_sof_intel_hda
snd_hwdep              20480  1 snd_hda_codec
snd_soc_core          385024  4 soundwire_intel,snd_sof,snd_sof_intel_hda_common,snd_soc_hdac_hda
snd_compress           24576  1 snd_soc_core
ac97_bus               12288  1 snd_soc_core
snd_pcm_dmaengine      16384  1 snd_soc_core
snd_pcm               172032  9 snd_hda_codec,soundwire_intel,snd_sof,snd_sof_intel_hda_common,snd_compress,snd_soc_core,snd_sof_utils,snd_hda_core,snd_pcm_dmaengine
snd_seq_midi           20480  0
snd_seq_midi_event     16384  1 snd_seq_midi
snd_rawmidi            49152  1 snd_seq_midi
joydev                 24576  0
snd_seq                98304  2 snd_seq_midi,snd_seq_midi_event
input_leds             12288  0
snd_seq_device         16384  3 snd_seq,snd_seq_midi,snd_rawmidi
snd_timer              49152  2 snd_seq,snd_pcm
snd                   135168  10 snd_seq,snd_seq_device,snd_hwdep,snd_hda_codec,snd_sof,snd_timer,snd_compress,snd_soc_core,snd_pcm,snd_rawmidi
wmi_bmof               12288  0
soundcore              16384  1 snd
ucsi_acpi              12288  0
typec_ucsi             53248  1 ucsi_acpi
mac_hid                12288  0
typec                 102400  1 typec_ucsi
acpi_tad               20480  0
acpi_pad              184320  0
sch_fq_codel           24576  6
msr                    12288  0
parport_pc             49152  0
ppdev                  24576  0
lp                     20480  0
parport                77824  3 parport_pc,lp,ppdev
ramoops                32768  0
reed_solomon           20480  1 ramoops
efi_pstore             12288  0
sunrpc                778240  1
ip_tables              28672  2 iptable_filter,iptable_nat
x_tables               53248  6 xt_conntrack,iptable_filter,xt_addrtype,ip_tables,iptable_nat,xt_MASQUERADE
autofs4                53248  2
btrfs                1785856  0
blake2b_generic        20480  0
raid10                 69632  0
raid456               176128  0
async_raid6_recov      20480  1 raid456
async_memcpy           16384  2 raid456,async_raid6_recov
async_pq               20480  2 raid456,async_raid6_recov
async_xor              16384  3 async_pq,raid456,async_raid6_recov
async_tx               16384  5 async_pq,async_memcpy,async_xor,raid456,async_raid6_recov
xor                    20480  2 async_xor,btrfs
raid6_pq              118784  4 async_pq,btrfs,raid456,async_raid6_recov
libcrc32c              12288  4 nf_conntrack,nf_nat,btrfs,raid456
raid1                  57344  0
raid0                  24576  0
multipath              16384  0
linear                 16384  0
crct10dif_pclmul       12288  1
crc32_pclmul           12288  0
ghash_clmulni_intel    16384  0
hid_generic            12288  0
sha512_ssse3           49152  0
aesni_intel           356352  0
nvme                   53248  5
usbhid                 69632  0
igb                   274432  0
crypto_simd            16384  1 aesni_intel
hid                   163840  2 usbhid,hid_generic
e1000e                319488  0
cryptd                 24576  2 crypto_simd,ghash_clmulni_intel
nvme_core             167936  6 nvme
dca                    16384  1 igb
i2c_algo_bit           12288  2 igb,xe
wmi                    40960  2 video,wmi_bmof
