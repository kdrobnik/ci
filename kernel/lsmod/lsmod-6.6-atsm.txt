Module                  Size  Used by
xe                   2482176  0
drm_exec               16384  1 xe
drm_gpuvm              24576  1 xe
gpu_sched              61440  1 xe
drm_buddy              20480  1 xe
drm_suballoc_helper    16384  1 xe
drm_ttm_helper         12288  1 xe
ttm                   110592  2 drm_ttm_helper,xe
drm_display_helper    233472  1 xe
cec                    94208  2 drm_display_helper,xe
rc_core                73728  1 cec
video                  73728  1 xe
xt_conntrack           12288  1
nft_chain_nat          12288  3
xt_MASQUERADE          16384  1
nf_nat                 61440  2 nft_chain_nat,xt_MASQUERADE
nf_conntrack_netlink    53248  0
nf_conntrack          196608  4 xt_conntrack,nf_nat,nf_conntrack_netlink,xt_MASQUERADE
nf_defrag_ipv6         24576  1 nf_conntrack
nf_defrag_ipv4         12288  1 nf_conntrack
xfrm_user              53248  1
xt_addrtype            12288  2
nft_compat             20480  4
nf_tables             356352  57 nft_compat,nft_chain_nat
nfnetlink              20480  4 nft_compat,nf_conntrack_netlink,nf_tables
br_netfilter           32768  0
bridge                393216  1 br_netfilter
stp                    12288  1 bridge
llc                    16384  2 bridge,stp
overlay               196608  0
sunrpc                786432  1
binfmt_misc            24576  1
intel_rapl_msr         16384  0
intel_rapl_common      36864  1 intel_rapl_msr
intel_uncore_frequency    12288  0
intel_uncore_frequency_common    16384  1 intel_uncore_frequency
i10nm_edac             32768  0
nfit                   77824  1 i10nm_edac
nls_iso8859_1          12288  2
x86_pkg_temp_thermal    16384  0
intel_powerclamp       24576  0
coretemp               24576  0
kvm_intel             466944  0
kvm                  1347584  1 kvm_intel
ipmi_ssif              45056  0
irqbypass              12288  1 kvm
rapl                   20480  0
intel_cstate           20480  0
cmdlinepart            12288  0
spi_nor               131072  0
joydev                 32768  0
isst_if_mbox_pci       12288  0
input_leds             12288  0
mei_me                 53248  0
isst_if_mmio           12288  0
isst_if_common         24576  2 isst_if_mmio,isst_if_mbox_pci
mei                   163840  1 mei_me
mtd                    98304  3 spi_nor,cmdlinepart
ioatdma                81920  0
intel_pch_thermal      20480  0
intel_vsec             20480  0
acpi_ipmi              20480  0
ipmi_si                86016  1
ipmi_devintf           16384  0
ipmi_msghandler        86016  4 ipmi_devintf,ipmi_si,acpi_ipmi,ipmi_ssif
acpi_pad              184320  0
acpi_power_meter       20480  0
mac_hid                12288  0
dm_multipath           45056  0
scsi_dh_rdac           16384  0
sch_fq_codel           24576  256
scsi_dh_emc            12288  0
scsi_dh_alua           20480  0
msr                    12288  0
parport_pc             53248  0
ppdev                  24576  0
lp                     28672  0
parport                77824  3 parport_pc,lp,ppdev
efi_pstore             12288  0
ip_tables              32768  0
x_tables               65536  5 xt_conntrack,nft_compat,xt_addrtype,ip_tables,xt_MASQUERADE
autofs4                53248  2
btrfs                1888256  0
blake2b_generic        24576  0
raid10                 69632  0
raid456               184320  0
async_raid6_recov      20480  1 raid456
async_memcpy           16384  2 raid456,async_raid6_recov
async_pq               20480  2 raid456,async_raid6_recov
async_xor              16384  3 async_pq,raid456,async_raid6_recov
async_tx               16384  5 async_pq,async_memcpy,async_xor,raid456,async_raid6_recov
xor                    20480  2 async_xor,btrfs
raid6_pq              126976  4 async_pq,btrfs,raid456,async_raid6_recov
libcrc32c              12288  5 nf_conntrack,nf_nat,btrfs,nf_tables,raid456
raid1                  57344  0
raid0                  24576  0
multipath              16384  0
linear                 16384  0
nvme                   57344  0
nvme_core             200704  1 nvme
nvme_common            24576  1 nvme_core
hid_generic            12288  0
usbhid                 73728  0
hid                   176128  2 usbhid,hid_generic
ixgbe                 471040  0
ast                    81920  0
i2c_algo_bit           16384  2 ast,xe
drm_shmem_helper       24576  1 ast
drm_kms_helper        262144  5 ast,drm_display_helper,xe
crct10dif_pclmul       12288  1
crc32_pclmul           12288  0
polyval_clmulni        12288  0
polyval_generic        12288  1 polyval_clmulni
dax_hmem               16384  0
cxl_acpi               24576  0
ghash_clmulni_intel    16384  0
xfrm_algo              20480  2 xfrm_user,ixgbe
drm                   745472  12 gpu_sched,drm_kms_helper,drm_exec,drm_suballoc_helper,ast,drm_shmem_helper,drm_display_helper,drm_buddy,drm_ttm_helper,xe,ttm
i40e                  593920  0
sha512_ssse3           45056  0
cxl_core              266240  1 cxl_acpi
aesni_intel           356352  0
crypto_simd            16384  1 aesni_intel
dca                    16384  2 ioatdma,ixgbe
cryptd                 24576  2 crypto_simd,ghash_clmulni_intel
ahci                   49152  5
i2c_i801               36864  0
mdio                   16384  1 ixgbe
xhci_pci               24576  0
spi_intel_pci          12288  0
libahci                53248  1 ahci
spi_intel              32768  1 spi_intel_pci
i2c_smbus              16384  1 i2c_i801
xhci_pci_renesas       16384  1 xhci_pci
wmi                    40960  1 video
