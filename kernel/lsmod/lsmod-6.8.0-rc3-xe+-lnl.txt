Module                  Size  Used by
xt_conntrack           12288  1
nft_chain_nat          12288  3
xt_MASQUERADE          16384  1
nf_nat                 61440  2 nft_chain_nat,xt_MASQUERADE
nf_conntrack_netlink    57344  0
nf_conntrack          200704  4 xt_conntrack,nf_nat,nf_conntrack_netlink,xt_MASQUERADE
nf_defrag_ipv6         24576  1 nf_conntrack
nf_defrag_ipv4         12288  1 nf_conntrack
xfrm_user              57344  1
xfrm_algo              20480  1 xfrm_user
xt_addrtype            12288  2
nft_compat             20480  4
nf_tables             368640  57 nft_compat,nft_chain_nat
libcrc32c              12288  3 nf_conntrack,nf_nat,nf_tables
nfnetlink              20480  4 nft_compat,nf_conntrack_netlink,nf_tables
br_netfilter           32768  0
bridge                405504  1 br_netfilter
stp                    12288  1 bridge
llc                    16384  2 bridge,stp
overlay               208896  0
input_leds             12288  0
joydev                 32768  0
hid_generic            12288  0
usbhid                 77824  0
hid                   176128  2 usbhid,hid_generic
idma64                 20480  0
sunrpc                802816  1
binfmt_misc            24576  1
snd_sof_pci_intel_lnl    12288  0
nls_iso8859_1          12288  2
snd_sof_intel_hda_common   217088  1 snd_sof_pci_intel_lnl
intel_uncore_frequency    16384  0
soundwire_intel        73728  1 snd_sof_intel_hda_common
intel_uncore_frequency_common    16384  1 intel_uncore_frequency
soundwire_generic_allocation    12288  1 soundwire_intel
snd_sof_intel_hda_mlink    45056  2 soundwire_intel,snd_sof_intel_hda_common
soundwire_cadence      40960  1 soundwire_intel
snd_sof_intel_hda      24576  1 snd_sof_intel_hda_common
snd_sof_pci            24576  2 snd_sof_pci_intel_lnl,snd_sof_intel_hda_common
snd_sof_xtensa_dsp     12288  1 snd_sof_intel_hda_common
snd_sof               372736  3 snd_sof_pci,snd_sof_intel_hda_common,snd_sof_intel_hda
x86_pkg_temp_thermal    16384  0
intel_powerclamp       24576  0
snd_sof_utils          16384  1 snd_sof
snd_soc_hdac_hda       24576  1 snd_sof_intel_hda_common
snd_hda_ext_core       36864  4 snd_sof_intel_hda_common,snd_soc_hdac_hda,snd_sof_intel_hda_mlink,snd_sof_intel_hda
coretemp               24576  0
snd_soc_acpi_intel_match   102400  2 snd_sof_pci_intel_lnl,snd_sof_intel_hda_common
snd_soc_acpi           16384  2 snd_soc_acpi_intel_match,snd_sof_intel_hda_common
snd_intel_dspcfg       36864  2 snd_sof,snd_sof_intel_hda_common
snd_intel_sdw_acpi     16384  2 snd_sof_intel_hda_common,snd_intel_dspcfg
kvm_intel             471040  0
snd_hda_codec         208896  2 snd_soc_hdac_hda,snd_sof_intel_hda
snd_hda_core          151552  5 snd_hda_ext_core,snd_hda_codec,snd_sof_intel_hda_common,snd_soc_hdac_hda,snd_sof_intel_hda
wmi_bmof               12288  0
snd_hwdep              20480  1 snd_hda_codec
soundwire_bus         110592  3 soundwire_intel,soundwire_generic_allocation,soundwire_cadence
kvm                  1396736  1 kvm_intel
snd_soc_core          446464  4 soundwire_intel,snd_sof,snd_sof_intel_hda_common,snd_soc_hdac_hda
snd_compress           24576  1 snd_soc_core
ac97_bus               12288  1 snd_soc_core
snd_pcm_dmaengine      16384  1 snd_soc_core
irqbypass              12288  1 kvm
snd_pcm               196608  9 snd_hda_codec,soundwire_intel,snd_sof,snd_sof_intel_hda_common,snd_compress,snd_soc_core,snd_sof_utils,snd_hda_core,snd_pcm_dmaengine
crct10dif_pclmul       12288  1
crc32_pclmul           12288  0
polyval_clmulni        12288  0
polyval_generic        12288  1 polyval_clmulni
ghash_clmulni_intel    16384  0
sha512_ssse3           49152  0
sha256_ssse3           32768  0
sha1_ssse3             32768  0
aesni_intel           356352  0
crypto_simd            16384  1 aesni_intel
snd_seq_midi           24576  0
cryptd                 24576  2 crypto_simd,ghash_clmulni_intel
snd_seq_midi_event     16384  1 snd_seq_midi
snd_rawmidi            53248  1 snd_seq_midi
snd_seq                98304  2 snd_seq_midi,snd_seq_midi_event
snd_seq_device         16384  3 snd_seq,snd_seq_midi,snd_rawmidi
snd_timer              49152  2 snd_seq,snd_pcm
snd                   143360  10 snd_seq,snd_seq_device,snd_hwdep,snd_hda_codec,snd_sof,snd_timer,snd_compress,snd_soc_core,snd_pcm,snd_rawmidi
soundcore              16384  1 snd
xhci_pci               24576  0
e1000e                348160  0
thunderbolt           495616  0
ucsi_acpi              12288  0
xhci_pci_renesas       16384  1 xhci_pci
intel_lpss_pci         24576  0
typec_ucsi             61440  1 ucsi_acpi
typec                 110592  1 typec_ucsi
mac_hid                12288  0
video                  73728  0
ov13b10                24576  0
v4l2_fwnode            36864  1 ov13b10
wmi                    32768  2 video,wmi_bmof
v4l2_async             28672  2 v4l2_fwnode,ov13b10
intel_skl_int3472_tps68470    20480  0
tps68470_regulator     12288  0
videodev              364544  3 v4l2_async,v4l2_fwnode,ov13b10
intel_pmc_core        118784  0
clk_tps68470           12288  0
intel_vsec             20480  1 intel_pmc_core
pmt_telemetry          16384  1 intel_pmc_core
mc                     86016  3 v4l2_async,videodev,ov13b10
acpi_tad               20480  0
intel_skl_int3472_discrete    24576  0
pmt_class              16384  1 pmt_telemetry
acpi_pad              184320  0
dm_multipath           45056  0
scsi_dh_rdac           16384  0
scsi_dh_emc            12288  0
scsi_dh_alua           20480  0
sch_fq_codel           24576  3
msr                    12288  0
parport_pc             53248  0
ppdev                  24576  0
lp                     28672  0
parport                77824  3 parport_pc,lp,ppdev
efi_pstore             12288  0
drm                   757760  0
ip_tables              32768  0
x_tables               65536  5 xt_conntrack,nft_compat,xt_addrtype,ip_tables,xt_MASQUERADE
autofs4                57344  2
